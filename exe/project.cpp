// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <thoth/thoth.hpp>
#include <thoth/window.hpp>

#include "../include/main.hpp"


int main()
{
	// Function from src/main.cpp
	test_function();
	
	std::cout << "Welcome to my project with Thōth " << thoth::version() << std::endl;
	
	// Window
	thoth::window window({ 800, 600 }, "Window title");
	
	while (window.is_open())
	{
		// Time elapsed
		auto const time_elapsed = float(window.time_elapsed());
		
		// Event
		while (thoth::event const event = window.event())
		{
			// Window
			if (event.window_closed()) { window.close(); }
			else if (auto const size = event.window_resized()) { std::cout << "Window resized " << *size << std::endl; }
			else if (event.focus_lost()) { std::cout << "Focus lost" << std::endl; }
			else if (event.focus_gained()) { std::cout << "Focus gained" << std::endl; }
			// Text
			else if (auto const text = event.text_entered()) { std::cout << "Text = " << *text << std::endl; }
			// Keyboard
			else if (auto const key = event.key_pressed()) { std::cout << "Key pressed = " << *key << std::endl; }
			else if (auto const key = event.key_released()) { std::cout << "Key released = " << *key << std::endl; }
			// Mouse
			else if (event.mouse_moved()) { std::cout << "Mouse moved" << std::endl; }
			else if (event.mouse_entered()) { std::cout << "Mouse entered" << std::endl; }
			else if (event.mouse_left()) { std::cout << "Mouse left" << std::endl; }
			// Mouse button
			else if (auto const button = event.mouse_button_pressed()) { std::cout << "Mouse button pressed = " << *button << std::endl; }
			else if (auto const button = event.mouse_button_released()) { std::cout << "Mouse button released = " << *button << std::endl; }
			// Mouse wheel
			else if (auto const delta = event.mouse_wheel_moved()) { std::cout << "Mouse wheel = " << *delta << std::endl; }
		}
		
		// Keyboard
		if (thoth::key_pressed(thoth::keyboard::up)) { std::cout << "Up key is pressed" << std::endl; }
		if (thoth::key_pressed(thoth::keyboard::down)) { std::cout << "Down key is pressed" << std::endl; }
		if (thoth::key_pressed(thoth::keyboard::left)) { std::cout << "Left key is pressed" << std::endl; }
		if (thoth::key_pressed(thoth::keyboard::right)) { std::cout << "Right key is pressed" << std::endl; }
		
		// Clear
		window.clear();
		
		// Draw
		// TODO
		
		// Update
		window.update();
	}
	
	return 0;
}
